package ferraro.leonardo.magnatocontrol;

import android.opengl.GLES20;
import android.opengl.GLSurfaceView;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

/**
 * Created by leonardo on 18/03/15.
 */
public class DemoRenderer implements GLSurfaceView.Renderer {

    static {
        System.loadLibrary("glrender");
    }

    public void onDrawFrame(GL10 gl){

        render();

    }

    public void onSurfaceChanged(GL10 gl,int width, int height){

        resize(width,height);

    }

    public void onSurfaceCreated(GL10 gl, EGLConfig config){

        init();

    }

    static native void init();
    static native void resize(int width,int height);
    static native void render();

}
