package ferraro.leonardo.magnatocontrol;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;


public class MainActivity extends ActionBarActivity {

    TextView logtv;

    Button drivebt;
    Button gallerybt;
    Button button3;
    Button settingsbt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        logtv = (TextView) this.findViewById(R.id.logtv);

        drivebt = (Button) this.findViewById(R.id.drivebt);
        gallerybt = (Button) this.findViewById(R.id.gallerybt);
        button3 = (Button) this.findViewById(R.id.button3);
        settingsbt = (Button) this.findViewById(R.id.settingsbt);

        drivebt.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, DriveActivity.class);
                Log.d("bt","Drive button pressed");
                startActivity(intent);
            }
        });

        /*gallerybt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logtv.setText("Galleria");
            }
        }); */

        settingsbt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logtv.setText("Impostazioni");
            }
        });


    }

}
