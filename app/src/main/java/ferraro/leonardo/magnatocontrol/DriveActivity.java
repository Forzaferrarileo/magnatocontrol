package ferraro.leonardo.magnatocontrol;

import android.app.Activity;
import android.opengl.GLSurfaceView;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;


public class DriveActivity extends Activity implements SurfaceHolder.Callback {

    private GLSurfaceView view;
    private float x,y;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        nativeCreate();

        setContentView(R.layout.activity_drive);

        SurfaceView surfaceview = (SurfaceView)findViewById(R.id.surfaceview);
        surfaceview.getHolder().addCallback(this);
        surfaceview.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                switch (event.getAction()) {

                    case MotionEvent.ACTION_DOWN:
                        nativeSetXY((int)event.getRawX(), (int)event.getRawY());
                        //Log.d("DAct","Premuto");
                        return true;
                    case MotionEvent.ACTION_MOVE:
                        nativeSetXY((int)event.getRawX(), (int)event.getRawY());
                        //Log.d("DAct","Mosso");
                        return true;
                    case MotionEvent.ACTION_UP:
                        nativeSetXY((int)event.getRawX(), (int)event.getRawY());
                        //Log.d("DAct","Rilasciato");
                        return true;

                }

                return false;
            }
        });
    }

    @Override
    protected void onPause(){
        super.onPause();
        nativePause();
    }

    @Override
    protected void onResume(){
        super.onResume();
        nativeResume();
    }

    @Override
    protected void onStop() {
        super.onDestroy();
        nativeStop();
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {

    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

        nativeSetSurface(holder.getSurface());

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        nativeSetSurface(null);
    }

    static native void nativeCreate();
    static native void nativeResume();
    static native void nativePause();
    static native void nativeStop();
    static native void nativeSetSurface(Object surface);
    static native void nativeSetXY(int x, int y);

    static {

        System.loadLibrary("glrender");

    }
}
