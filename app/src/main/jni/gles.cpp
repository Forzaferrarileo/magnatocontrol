#include <jni.h>
#include <android/native_window.h>
#include <android/log.h>
#include <math.h>
#include "gles.h"
#include "texture.h"

#define PI 3.141592

float modelMat[16] = { 1.0, 0.0, 0.0, 0.0,
                       0.0, 1.0, 0.0, 0.0,
                       0.0, 0.0, 1.0, 0.0,
                       0.0, 0.0, 0.0, 1.0 };

static float quadverts[] = {
  -100.5f, -100.5f, 0.0f,  // 0. left-bottom
   100.5f, -100.5f, 0.0f,  // 1. right-bottom
  -100.5f,  100.5f, 0.0f,  // 2. left-top
   100.5f,  100.5f, 0.0f   // 3. right-top
};

static float texcoords[]{
    0.0f,    0.0f,
    1.0f,    0.0f,
    1.0f,    1.0f,
    0.0f,    1.0f
};    

GLubyte indices[] = {
    0, 4, 5,    0, 5, 1,
    1, 5, 6,    1, 6, 2,
    2, 6, 7,    2, 7, 3,
    3, 7, 4,    3, 4, 0,
    4, 7, 6,    4, 6, 5,
    3, 0, 1,    3, 1, 2
};

static const char gVertexShader[] =
    "attribute vec3 vPosition;\n"
    "uniform mat4 projMatrix;\n"
    "uniform mat4 viewMatrix;\n"
    "uniform mat4 modelMatrix;\n"
    "attribute vec2 atexcoord;\n"
    "varying vec2 texcoord;\n"
    "void main() {\n"
    "  gl_Position = projMatrix * viewMatrix * modelMatrix * vec4(vPosition,1.0);\n"
    "  texcord = atexcoord;\n"    
    //"gl_Position = vPosition;\n"
    "}\n";

static const char gFragmentShader[] =
    "varying vec2 texcoord;\n"
    "uniform sampler2D tex;\n"
    "void main() {\n"
    "  gl_FragColor = texture2D(tex,texcoord);\n"
    "}\n";


Renderer::Renderer(){

    pthread_mutex_init(&mutex, 0);

    display = 0;
    surface = 0;
    context = 0;
    x = 0.0;
    y = 0.0;

}

Renderer::~Renderer(){

    pthread_mutex_destroy(&mutex);

}

void Renderer::start(){

    pthread_create(&thread, NULL, threadStartCallback, this);

}

void Renderer::stop(){

    pthread_mutex_lock(&mutex);
    msg = MSG_LOOP_EXIT ;
    pthread_mutex_unlock(&mutex);

    pthread_join(thread,0);

}

void Renderer::ortho(float left, float right, float top, float bottom, float near, float far){

    float tx = ((right+left)/(right-left))*-1;
    float ty = ((top+bottom)/(top-bottom))*-1;
    float tz = ((far+near)/(far-near))*-1;

    projMat[0] = 2.0/(right-left);
    projMat[1] = 0.0f;
    projMat[2] = 0.0f;
    projMat[3] = 0.0f;
    projMat[4] = 0.0f;
    projMat[5] = 2.0/(top-bottom);
    projMat[6] = 0.0f;
    projMat[7] = 0.0f;
    projMat[8] = 0.0f;
    projMat[9] = 0.0f;
    projMat[10] = (2.0*-1)/(far-near);
    projMat[11] = 0.0f;
    projMat[12] = tx;
    projMat[13] = ty;
    projMat[14] = tz;
    projMat[15] = 1.0f;

}

void Renderer::lookAt(float eye[3], float at[3], float up[3]){

    float forward[3], up_norm[3], side[3];
    float len;

    /*** Calculate forward vector ***/

    forward[0] = eye[0] - at[0];
    forward[1] = eye[1] - at[1];
    forward[2] = eye[2] - at[2];

    /*** Normalize forward vector ***/

    len = sqrt( forward[0]*forward[0] + forward[1]*forward[1] + forward[2]*forward[2]);

    forward[0] = forward[0] / len ;
    forward[1] = forward[1] / len ;
    forward[2] = forward[2] / len ;

    /*** Normalize up vector ***/

    memcpy( up, up_norm, sizeof(up));
    len = sqrt( up_norm[0]*up_norm[0] + up_norm[1]*up_norm[1] + up_norm[2]*up_norm[2]);

    up_norm[0] = up_norm[0] / len;
    up_norm[1] = up_norm[1] / len;
    up_norm[2] = up_norm[2] / len;

    /*** Calcolate side vector using cross product between up_norm and forward ***/

    side[0] = up_norm[1]*forward[2] - up_norm[2]*forward[1];
    side[1] = up_norm[2]*forward[0] - up_norm[0]*forward[2];
    side[2] = up_norm[0]*forward[1] - up_norm[1]*forward[0];

    /*** Recalculate up_norm vector usign cross product between side and forward ***/

    up_norm[0] = forward[1]*side[2] - forward[2]*side[1];
    up_norm[1] = forward[2]*side[0] - forward[0]*side[2];
    up_norm[2] = forward[0]*side[1] - forward[1]*side[0];

    /*** Construct lookatmatrix ***/

    lookAtMat[0] = side[0];
    lookAtMat[4] = up_norm[0];
    lookAtMat[8] = forward[0] * -1;
    lookAtMat[12]= 0.0f;
    lookAtMat[1] = side[1];
    lookAtMat[5] = up_norm[1];
    lookAtMat[9] = forward[1] * -1;
    lookAtMat[13]= 0.0f;
    lookAtMat[2] = side[2];
    lookAtMat[6] = up_norm[2];
    lookAtMat[10]= forward[2] * -1;
    lookAtMat[14]= 0.0f;
    lookAtMat[3] = 0.0f;
    lookAtMat[7] = 0.0f;
    lookAtMat[11]= 0.0f;
    lookAtMat[15]= 1.0f;


    /***  Post Translate camera to origin ***/

    lookAtMat[12] += ((eye[0]*lookAtMat[0]) + (eye[1]*lookAtMat[4]) + (eye[2]*lookAtMat[8]))*-1;
    lookAtMat[13] += ((eye[0]*lookAtMat[1]) + (eye[1]*lookAtMat[5]) + (eye[2]*lookAtMat[9]))*-1;
    lookAtMat[14] += (eye[0]*lookAtMat[2]) + (eye[1]*lookAtMat[6]) + (eye[2]*lookAtMat[10]);
    //lookAtMat[15] += (eye[0]*lookAtMat[3]) + (eye[1]*lookAtMat[7]) + (eye[2]*lookAtMat[11]);

}

GLuint Renderer::loadShader(GLenum shaderType, const char* source) {

    GLuint shader = glCreateShader(shaderType);

    if(shader){

        glShaderSource(shader, 1, &source, NULL);
        glCompileShader(shader);
        GLint compiled = 0;
        glGetShaderiv(shader, GL_COMPILE_STATUS, &compiled);
        if(compiled){
            return shader;
        }
        else{
            return 0;
        }
    }
}

GLuint Renderer::createProgram(const char* vertexSource, const char* fragmentSource){

    GLuint vertexShader = loadShader(GL_VERTEX_SHADER, vertexSource);
    if(!vertexShader){
        return 0;
    }

    GLuint fragmentShader = loadShader(GL_FRAGMENT_SHADER, fragmentSource);
    if(!fragmentShader){
        return 0;
    }

    GLuint program = glCreateProgram();
    if(program){

        glAttachShader(program, vertexShader);
        glAttachShader(program, fragmentShader);
        glLinkProgram(program);

        GLint linkStatus = GL_FALSE;
        glGetProgramiv(program, GL_LINK_STATUS, &linkStatus);
        if(linkStatus != GL_TRUE){
            return 0;
        }else{
            return program;
        }

    }else{
        return 0;
    }

}

void Renderer::setWindow(ANativeWindow *mwindow){

    pthread_mutex_lock(&mutex);
    msg = MSG_WINDOW_SET ;
    window = mwindow;
    pthread_mutex_unlock(&mutex);

}

void Renderer::renderLoop(){

    bool renderingEnabled = true;

    while(renderingEnabled){

        pthread_mutex_lock(&mutex);

        switch(msg){

            case MSG_WINDOW_SET:
                initialize();
                break;

            case MSG_LOOP_EXIT:
                renderingEnabled = false;
                destroy();
                break;

            default:
                break;

        }

        msg = MSG_NONE ;

        if(display){
            drawFrame();
            if(!eglSwapBuffers(display, surface)){
                __android_log_print(ANDROID_LOG_VERBOSE, APP, "Eglswaperror %d", eglGetError());
            }
        }

        pthread_mutex_unlock(&mutex);

    }

}

bool Renderer::initialize(){

    const EGLint attribs[] = {
        EGL_RENDERABLE_TYPE, EGL_OPENGL_ES2_BIT,
        EGL_SURFACE_TYPE, EGL_WINDOW_BIT,
        EGL_BLUE_SIZE, 8,
        EGL_GREEN_SIZE, 8,
        EGL_RED_SIZE, 8,
        EGL_NONE
    };

    EGLDisplay _display;
    EGLConfig config;
    EGLint numConfigs;
    EGLint format;
    EGLSurface _surface;
    EGLContext _context;
    EGLint _width;
    EGLint _height;
    GLfloat ratio;
    GLfloat ratio2;

    if((_display = eglGetDisplay(EGL_DEFAULT_DISPLAY)) == EGL_NO_DISPLAY){
        return false;
    }
    if(!eglInitialize(_display, 0, 0)){
        return false;
    }
    if(!eglChooseConfig(_display, attribs, &config, 1, &numConfigs)){
        destroy();
        return false;
    }
    if(!eglGetConfigAttrib(_display, config, EGL_NATIVE_VISUAL_ID, &format)){
        destroy();
        return false;
    }

    ANativeWindow_setBuffersGeometry(window,0,0,format);

    if(!(_surface = eglCreateWindowSurface(_display, config, window, 0))){
        destroy();
        return false;
    }

    EGLint contextAttrs[] = {
        EGL_CONTEXT_CLIENT_VERSION, 2,
        EGL_NONE
    };

    if(!(_context = eglCreateContext(_display,config, 0, contextAttrs))){
        destroy();
        return false;
    }

    if( !eglMakeCurrent(_display, _surface, _surface, _context)){
        destroy();
        return false;
    }

    if(!eglQuerySurface(_display, _surface, EGL_WIDTH, &_width) ||
       !eglQuerySurface(_display, _surface, EGL_HEIGHT, &_height)){

        destroy();
        return false;

    }

    display = _display;
    surface = _surface;
    context = _context;
    width = _width;
    height = _height;

    glEnable(GL_CULL_FACE);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_TEXTURE_2D);
    glShadeModel(GL_SMOOTH);

    glClearColor(50,0,0,0);

    mProgram = createProgram(gVertexShader, gFragmentShader);
    mvPositionHandle = glGetAttribLocation(mProgram, "vPosition");
    matexcoord = glGetAttribLocation(mProgram, "atexcoord");
    mPerspectivehandler = glGetUniformLocation(mProgram, "projMatrix");
    mViewHandler = glGetUniformLocation(mProgram, "viewMatrix");
    mModelHandler = glGetUniformLocation(mProgram, "modelMatrix");
    textureHandler = glGetUniformLocation(mProgram, "tex");

    glViewport(0, 0, width, height);

    ortho(0.0,width,0.0, height,-1.0,50.0);

    float camEye[3] = { 0.0, 0.0, 0.0};
    float camAt[3] = { 0.0, 0.0, 700.0};
    float camUp[3] = { 0.0, 1.0, 0.0};

    lookAt(camEye, camAt, camUp);

    quadverts[0] = -width/2;
    quadverts[1] = -height/2;
    quadverts[3] =  width/2;
    quadverts[4] = -height/2;
    quadverts[6] = -width/2;
    quadverts[7] =  height/2;
    quadverts[9] =  width/2;
    quadverts[10]=  height/2;

    x = width/2;
    y= height/2;

    texture = loadBmpTex("/sdcard1/tex.data");

    return true;

}

void Renderer::destroy(){

    eglMakeCurrent(display, EGL_NO_SURFACE, EGL_NO_SURFACE, EGL_NO_CONTEXT);
    eglDestroyContext(display, context);
    eglDestroySurface(display,surface);
    eglTerminate(display);

    display = EGL_NO_DISPLAY;
    surface = EGL_NO_SURFACE;
    context = EGL_NO_CONTEXT;
    width = 0;
    height = 0;

}

void Renderer::drawFrame(){

    modelMat[12] = x;
    modelMat[13] = y * -1;

    // 3D drawing
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glUseProgram(mProgram);
    glUniformMatrix4fv(mPerspectivehandler, 1, GL_FALSE, projMat);
    glUniformMatrix4fv(mViewHandler, 1, GL_FALSE, lookAtMat);
    glUniformMatrix4fv(mModelHandler, 1, GL_FALSE, modelMat);

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, texture);
    glVertexAttribPointer(matexcoord, 2, GL_FLOAT, GL_FALSE, 0, texcoords);
    glUniform1i(textureHandler, 0);

    glEnableVertexAttribArray(mvPositionHandle);
    glVertexAttribPointer(mvPositionHandle, 3, GL_FLOAT, GL_FALSE, 0, quadverts);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    glDisableVertexAttribArray(mvPositionHandle);


}

void* Renderer::threadStartCallback(void* arg){

    Renderer *renderer = (Renderer*) arg;

    renderer->renderLoop();
    pthread_exit(0);

}











