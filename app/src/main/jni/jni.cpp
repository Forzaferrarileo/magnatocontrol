#include <jni.h>
#include <android/native_window.h>
#include <android/native_window_jni.h>
#include "gles.h"

static Renderer *renderer = 0;
static ANativeWindow *window = 0;

void nativeCreate(JNIEnv* env){

    renderer = new Renderer();

}

void nativeResume(JNIEnv* env){

    renderer->start();

}

void nativePause(JNIEnv* env){

    renderer->stop();

}

void nativeStop(JNIEnv* env){

    delete renderer;
    renderer = 0;

}

void nativeSetSurface(JNIEnv* env, jobject obj, jobject surface){

    if(surface != 0){

        window = ANativeWindow_fromSurface(env, surface);
        renderer->setWindow(window);

    }else{

        ANativeWindow_release(window);

    }

}

void getXY(JNIEnv* env, jobject obj, jint x, jint y){

    if (renderer != 0 ){

        renderer->x = x;
        renderer->y = y;

    }

}

static JNINativeMethod native_methods[] = {
	{ "nativeCreate", "()V", (void *) nativeCreate},
	{ "nativePause", "()V", (void *) nativePause},
	{ "nativeResume", "()V", (void *) nativeResume},
	{ "nativeStop", "()V", (void *) nativeStop},
	{ "nativeSetSurface", "(Ljava/lang/Object;)V", (void *) nativeSetSurface},
	{ "nativeSetXY", "(II)V", (void *) getXY}
};

jint JNI_OnLoad(JavaVM *vm, void *reserved) {

	JNIEnv *env;

	if ( vm->GetEnv((void**) &env, JNI_VERSION_1_4) != JNI_OK) {
		return 0;
	}
	jclass clazz = env->FindClass("ferraro/leonardo/magnatocontrol/DriveActivity");

	env->RegisterNatives (clazz, native_methods, 6);

	return JNI_VERSION_1_4;
}