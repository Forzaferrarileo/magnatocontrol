#ifndef RENDERER_H
#define RENDERER_H

#include <pthread.h>
#include <EGL/egl.h>
#include <GLES2/gl2.h>
#include <GLES2/gl2ext.h>
#include <GLES/gl.h>
#include <GLES/glext.h>

#define APP "MagnatoControl"

class Renderer {

public:

    Renderer();
    ~Renderer();

    void start();
    void stop();
    void setWindow(ANativeWindow* window);
    int x;
    int y;

private:

    enum RenderMessage {
        MSG_NONE = 0,
        MSG_WINDOW_SET,
        MSG_LOOP_EXIT
    };
    enum RenderMessage msg;

    pthread_t thread;
    pthread_mutex_t mutex;


    ANativeWindow* window;
    EGLDisplay display;
    EGLSurface surface;
    EGLContext context;

    int width;
    int height;
    int xscreen;
    bool decreasing;

    static void* threadStartCallback(void *arg);

    void renderLoop();
    bool initialize();
    void destroy();
    void drawFrame();

    GLuint loadShader(GLenum, const char*);
    GLuint createProgram(const char* , const char* );
    GLuint mProgram;
    GLuint mvPositionHandle;
    GLuint mPerspectivehandler;
    GLuint mViewHandler;
    GLuint mModelHandler;
    GLuint texture;
    GLuint textureHandler;

    void ortho(float, float, float, float, float, float);
    void lookAt(float[],float[],float[]);
    float lookAtMat[16];
    float projMat[16];
    //float modelMat[16];
   // static float identityMat[16];

};

#endif