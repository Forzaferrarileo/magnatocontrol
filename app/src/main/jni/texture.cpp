#include <stdio.h>
#include <stdlib.h>
#include "texture.h"

GLuint loadBmpTex(const char *path){

    unsigned int imageSize;
    unsigned int mheight;
    unsigned int mwidth;

    unsigned char *data; //Image data

    FILE *file = fopen(path,"rb");

    if(!file){
        __android_log_print(ANDROID_LOG_ERROR, APP, "%s Can't open file", path);
    }

    mwidth=512;
    mheight=512;
    imageSize = mwidth*mheight*3;

    data = new unsigned char[imageSize];

    // Read the actual data from the file into the buffer
    fread(data,1,imageSize,file);

    // Everything is in memory now, the file wan be closed
    fclose (file);

    GLuint textureID;

    glGenTextures(1, &textureID);
    glBindTexture(GL_TEXTURE_2D, textureID);

    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, mwidth, mheight, 0, GL_RGB, GL_UNSIGNED_BYTE, data);

    delete[] data;

    //Rough filtering ( nearest)
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

    __android_log_print(ANDROID_LOG_INFO, APP, "Texture created! : %i", textureID);

    return textureID;

}
